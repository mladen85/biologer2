package org.biologer.biologer;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.biologer.biologer.model.LoginResponse;
import org.biologer.biologer.model.UserData;
import org.biologer.biologer.model.network.UserDataResponse;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class LoginActivity extends AppCompatActivity  {

    EditText et_username;
    EditText et_password;
    TextView tv_wrongPass;
    TextView tv_register;
    //regex za email
    String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);
        tv_wrongPass = (TextView) findViewById(R.id.tv_wrongPass);
    }

    public void onLogin(View view) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(et_username.getText().toString());
        if (!(matcher.matches()))
        {
            tv_wrongPass.setText(R.string.valid_email);
            return;
        }
        if (!(et_password.getText().length() > 3))
        {
            tv_wrongPass.setText(R.string.valid_pass);
            return;
        }

        Call<LoginResponse> login = App.get().getService().login("password", "2", "eCyCQncCQOtkRln6KxWfvRt0Qq562mB12SNl3oi3", "*", et_username.getText().toString(), et_password.getText().toString());

        login.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {
                    LoginResponse response1 = response.body();
                    SettingsManager.setToken(response1.getAccessToken());
                    navDrawerFill();
                }
                else {
                    tv_wrongPass.setText(R.string.wrong_creds);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });

    }

    private void navDrawerFill(){
        Call<UserDataResponse> serv = App.get().getService().getUserData();
        serv.enqueue(new Callback<UserDataResponse>() {
            @Override
            public void onResponse(Call<UserDataResponse> serv, Response<UserDataResponse> response) {
                App.get().getDaoSession().getUserDataDao().deleteAll();
                String email = response.body().getData().getEmail();
                String name = response.body().getData().getFullName();
                UserData uData = new UserData(null, email, name);
                App.get().getDaoSession().getUserDataDao().insertOrReplace(uData);
                Intent intent = new Intent(LoginActivity.this, LandingActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<UserDataResponse> call, Throwable t) {
                String s = "ff";
            }
        });
    }


    public void onRegister(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://biologer.org/register"));
        startActivity(browserIntent);
    }

    public void onForgotPass(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://biologer.org/password/reset"));
        startActivity(browserIntent);
    }

}
