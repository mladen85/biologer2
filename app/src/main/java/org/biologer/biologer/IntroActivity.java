package org.biologer.biologer;

import android.app.Fragment;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import org.biologer.biologer.model.User;


public class IntroActivity extends AppIntro {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        addSlide(SampleSlide.newInstance(R.layout.slide_1));
//        addSlide(SampleSlide.newInstance(R.layout.slide_2));
//        addSlide(SampleSlide.newInstance(R.layout.slide_3));
//        addSlide(SampleSlide.newInstance(R.layout.slide_4));

        addSlide(AppIntroFragment.newInstance(getString(R.string.Slide1_title), getString(R.string.first_slide), R.drawable.slide1,
                ContextCompat.getColor(getApplicationContext(), R.color.slide1)));
        addSlide(AppIntroFragment.newInstance(getString(R.string.Slide2_title), getString(R.string.second_slide), R.drawable.slide2,
                ContextCompat.getColor(getApplicationContext(), R.color.slide2)));
        addSlide(AppIntroFragment.newInstance(getString(R.string.Slide3_title), getString(R.string.third_slide), R.drawable.slide3,
                ContextCompat.getColor(getApplicationContext(), R.color.slide3)));
        addSlide(AppIntroFragment.newInstance(getString(R.string.Slide4_title), getString(R.string.fourth_slide), R.drawable.slide4,
                ContextCompat.getColor(getApplicationContext(), R.color.slide4)));
    }

    @Override
    public void onSkipPressed(){
        if (User.getUser().isLoggedIn()) {
            Intent intent = new Intent(IntroActivity.this, LandingActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void onNextPressed() {
        // Do something when users tap on Next button.
    }

    @Override
    public void onDonePressed() {
        // Do something when users tap on Done button.
        if (User.getUser().isLoggedIn()) {
            Intent intent = new Intent(IntroActivity.this, LandingActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void onSlideChanged() {
        // Do something when slide is changed
    }
}
