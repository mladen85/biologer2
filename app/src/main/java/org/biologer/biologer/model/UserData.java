package org.biologer.biologer.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by brjovanovic on 3/20/2018.
 */

@Entity
public class UserData {
    @org.greenrobot.greendao.annotation.Id
    private Long Id;
    private String username;
    private String email;
    @Generated(hash = 1199205176)
    public UserData(Long Id, String username, String email) {
        this.Id = Id;
        this.username = username;
        this.email = email;
    }
    @Generated(hash = 1838565001)
    public UserData() {
    }
    public Long getId() {
        return this.Id;
    }
    public void setId(Long Id) {
        this.Id = Id;
    }
    public String getUsername() {
        return this.username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

}
